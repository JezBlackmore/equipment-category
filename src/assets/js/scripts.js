
const scriptsJs = () => {
    const functionGetLeftMargin = (leftMargin) => {

        if(!leftMargin){
            return 0;
        }
        const marginLeftCal1 = window.getComputedStyle(leftMargin).marginLeft;
        return parseInt(marginLeftCal1.replace(/\D/g, ""));
    }
    const functionGetGapWidth = (innerSlider) => {
        const gapWidth = window.getComputedStyle(innerSlider).gap;
        return parseInt(gapWidth.replace(/\D/g, ""));
    }

    const functionForScroll = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, slider5, totalDivWidth, marginLeftCalNumber) => {
        sliderFiveRight.disabled = false;

        const chooseWhereToDisableLeft = marginLeftCalNumber - 30 < 0? 0 : 30;

        if (sliderDiv.scrollLeft < marginLeftCalNumber) {
            sliderFiveLeft.disabled = true;
            sliderDiv.classList.remove('snapEffect');

        } else if (sliderDiv.scrollLeft > (totalDivWidth - sliderDiv.getBoundingClientRect().width) - chooseWhereToDisableLeft) {
            sliderFiveRight.disabled = true;
        } else {
            sliderFiveLeft.disabled = false;
            sliderFiveRight.disabled = false;
            if (!sliderToChange.disableSnapOnScroll) {
               /*  sliderDiv.classList.add('snapEffect'); */
            }
        }
    }

    const functionForResize = (sliderFiveLeft, sliderFiveRight, sliderDiv, totalDivWidth) => {
        if (sliderDiv.scrollLeft < 100) {
            sliderFiveLeft.disabled = true;
        } else if (sliderDiv.scrollLeft > (totalDivWidth - sliderDiv.getBoundingClientRect().width) - 30) {
            sliderFiveRight.disabled = true;
        } else {
            sliderFiveLeft.disabled = false;
            sliderFiveRight.disabled = false;
        }
    }


    const functionForLeftClick = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, slider5, gapWidthNumber, cardWidth) => {
        sliderToChange.disableSnapOnScroll = true;
        sliderDiv.classList.remove('snapEffect');
        sliderFiveRight.disabled = false;
        let count = 0;
        let leftOver = 0;

        if (sliderDiv.scrollLeft < marginLeftCalNumber) {
            leftOver = marginLeftCalNumber - sliderDiv.scrollLeft;
        }

        sliderDiv.scrollLeft = sliderDiv.scrollLeft + leftOver;

        const scrollEfect = setInterval(() => {
            if (sliderToChange.leftOverRightEnd > 0) {
                sliderDiv.scrollLeft = sliderDiv.scrollLeft - 6;
                sliderToChange.leftOverRightEnd = sliderToChange.leftOverRightEnd - 6;
            } else {
                sliderDiv.scrollLeft = sliderDiv.scrollLeft - 6;
                count = count + 6;
            }
            if (sliderDiv.scrollLeft <= 0) {
                clearInterval(scrollEfect);
                sliderFiveLeft.disabled = true;
                count = 0;
                console.log("SNAP111111")
               /*  sliderDiv.classList.add('snapEffect'); */
                sliderToChange.disableSnapOnScroll = false;
            } else if (count >= cardWidth + gapWidthNumber) {
                clearInterval(scrollEfect);
                count = 0;
                console.log("SNAP2222222")
                sliderDiv.classList.add('snapEffect');
                sliderToChange.disableSnapOnScroll = false;
            }
        }, 1);
    }

    const functionForRightClick = (sliderToChange, sliderFiveLeft, sliderFiveRight, sliderDiv, marginLeftCalNumber, totalDivWidth, cardWidth, gapWidthNumber, slider1) => {
        sliderDiv.classList.remove('snapEffect');
        sliderToChange.disableSnapOnScroll = true;
        sliderFiveLeft.disabled = false;
        let leftOver = 0;
        let count = 0;

        if (sliderDiv.scrollLeft < marginLeftCalNumber) {
            leftOver = marginLeftCalNumber - sliderDiv.scrollLeft;
        }

        sliderDiv.scrollLeft = sliderDiv.scrollLeft + leftOver;

        const scrollEfect = setInterval(() => {
            sliderDiv.scrollLeft = sliderDiv.scrollLeft + 6;
            count = count + 6;

            if (sliderDiv.scrollLeft >= totalDivWidth - sliderDiv.getBoundingClientRect().width) {
                clearInterval(scrollEfect);
                sliderFiveRight.disabled = true;
                sliderToChange.leftOverRightEnd = count;
                console.log("SNAP3333333")
                
                sliderDiv.classList.add('snapEffect');
                count = 0;
                sliderToChange.disableSnapOnScroll = false;

            } else if (count >= cardWidth + gapWidthNumber) {
                clearInterval(scrollEfect);
                count = 0;
                console.log("SNAP444444")
                sliderDiv.classList.add('snapEffect');
                sliderToChange.disableSnapOnScroll = false;
            }
        }, 1);
    }

    const addNewColor = (sliderID) => {        
        const cards = document.querySelectorAll(`#${sliderID} .productCard__image`);
        cards.forEach(item => {
            if(item.classList.contains('newColorsTrue')){
                item.innerHTML = `<div class="newColorTag">
                                    <span>New Colours</span>
                                </div>`;
            }
        })
    };

    /* Slider 1 */

    const slider1 = document.querySelector('#slider1 .slider_controls');
    const slider1Right = document.querySelector('#slider1 .sliderRight');
    const slider1Left = document.querySelector('#slider1 .sliderLeft');
    const card1 = document.querySelectorAll('#slider1 .card600x600');
    const sliderDiv1 = document.querySelector('#slider1 .sider_container');
    const innerSlider1 = document.querySelector('#slider1 .innerSlider');

    const marginLeft1 = document.querySelector('#slider1 .firstChildMargin');
    const marginLeftCalNumber1 = functionGetLeftMargin(marginLeft1);
    const totalDivWidth1 = innerSlider1.offsetWidth;
    const cardWidth1 = card1[0].offsetWidth;
    const gapWidthNumber1 = functionGetGapWidth(innerSlider1);

  

    const slider1obj = {
        leftOverRightEnd: 0,
        disableSnapOnScroll: false,
    }
    slider1Right.addEventListener('click', () => {
        functionForRightClick(slider1obj, slider1Left, slider1Right, sliderDiv1, marginLeftCalNumber1, totalDivWidth1, cardWidth1, gapWidthNumber1, slider1);
    })
    slider1Left.addEventListener('click', () => {
        functionForLeftClick(slider1obj, slider1Left, slider1Right, sliderDiv1, marginLeftCalNumber1, slider1, gapWidthNumber1, cardWidth1);
    })
    sliderDiv1.addEventListener('scroll', () => {
        functionForScroll(slider1obj, slider1Left, slider1Right, sliderDiv1, slider1, totalDivWidth1, marginLeftCalNumber1);
    })

 
    /* Slider 2 */

   
    const slider2 = document.querySelector('#slider2 .slider_controls');
    const slider2Right = document.querySelector('#slider2 .sliderRight');
    const slider2Left = document.querySelector('#slider2 .sliderLeft');
    const card2 = document.querySelectorAll('#slider2 .productCard');
    const sliderDiv2 = document.querySelector('#slider2 .sider_container');
    const innerSlider2 = document.querySelector('#slider2 .innerSlider');
    const marginLeft2 = document.querySelector('#slider2 .firstChildMargin');
    const marginLeftCalNumber2 = functionGetLeftMargin(marginLeft2);
    const totalDivWidth2 = innerSlider2.offsetWidth;
    const cardWidth2 = card2[0].offsetWidth;
    const gapWidthNumber2 = functionGetGapWidth(innerSlider2);

    const slider2obj = {
        leftOverRightEnd: 0,
        disableSnapOnScroll: false,
    }
    slider2Right.addEventListener('click', () => {
        functionForRightClick(slider2obj, slider2Left, slider2Right, sliderDiv2, marginLeftCalNumber2, totalDivWidth2, cardWidth2, gapWidthNumber2 );
    })
    slider2Left.addEventListener('click', () => {
        functionForLeftClick(slider2obj, slider2Left, slider2Right, sliderDiv2, marginLeftCalNumber2, slider2, gapWidthNumber2, cardWidth2);
    })
    sliderDiv2.addEventListener('scroll', () => {
        functionForScroll(slider2obj, slider2Left, slider2Right, sliderDiv2, slider2, totalDivWidth2, marginLeftCalNumber2);
    })


    addNewColor(`slider2`);

    /* Slider 3 */

   
    const slider3 = document.querySelector('#slider3 .slider_controls');
    const slider3Right = document.querySelector('#slider3 .sliderRight');
    const slider3Left = document.querySelector('#slider3 .sliderLeft');
    const card3 = document.querySelectorAll('#slider3 .productCard__image');
    const sliderDiv3 = document.querySelector('#slider3 .sider_container');
    const innerSlider3 = document.querySelector('#slider3 .innerSlider');
    const marginLeft3 = document.querySelector('#slider3 .firstChildMargin');
    const marginLeftCalNumber3 = functionGetLeftMargin(marginLeft3);
    const totalDivWidth3 = innerSlider3.offsetWidth;
    const cardWidth3 = card3[0].offsetWidth;
    const gapWidthNumber3 = functionGetGapWidth(innerSlider3);

    const slider3obj = {
        leftOverRightEnd: 0,
        disableSnapOnScroll: false,
    }
    slider3Right.addEventListener('click', () => {
        functionForRightClick(slider3obj, slider3Left, slider3Right, sliderDiv3, marginLeftCalNumber3, totalDivWidth3, cardWidth3, gapWidthNumber3);
    })
    slider3Left.addEventListener('click', () => {
        functionForLeftClick(slider3obj, slider3Left, slider3Right, sliderDiv3, marginLeftCalNumber3, slider3, gapWidthNumber3, cardWidth3);
    })
    sliderDiv3.addEventListener('scroll', () => {
        functionForScroll(slider3obj, slider3Left, slider3Right, sliderDiv3, slider1, totalDivWidth3, marginLeftCalNumber3);
    })


    /* Slider 4 */
    
    const slider4 = document.querySelector('#slider4 .slider_controls');
    const slider4Right = document.querySelector('#slider4 .sliderRight');
    const slider4Left = document.querySelector('#slider4 .sliderLeft');
    const card4 = document.querySelectorAll('#slider4 .blogCard');
    const sliderDiv4 = document.querySelector('#slider4 .sider_container');
    const innerSlider4 = document.querySelector('#slider4 .innerSlider');
    const marginLeft4 = document.querySelector('#slider4 .firstChildMargin');
    const marginLeftCalNumber4 = functionGetLeftMargin(marginLeft4);
    const totalDivWidth4 = innerSlider4.offsetWidth;
    const cardWidth4 = card4[0].offsetWidth;
    const gapWidthNumber4 = functionGetGapWidth(innerSlider4);

    const slider4obj = {
        leftOverRightEnd: 0,
        disableSnapOnScroll: false,
    }
    slider4Right.addEventListener('click', () => {
        functionForRightClick(slider4obj, slider4Left, slider4Right, sliderDiv4, marginLeftCalNumber4, totalDivWidth4, cardWidth4, gapWidthNumber4);
    })
    slider4Left.addEventListener('click', () => {
        functionForLeftClick(slider4obj, slider4Left, slider4Right, sliderDiv4, marginLeftCalNumber4, slider4, gapWidthNumber4, cardWidth4);
    })
    sliderDiv4.addEventListener('scroll', () => {
        functionForScroll(slider4obj, slider4Left, slider4Right, sliderDiv4, slider4, totalDivWidth4, marginLeftCalNumber4);
    })

    /* Slider 5 */

   
    const slider5 = document.querySelector('#slider5 .slider_controls');
    const slider5Right = document.querySelector('#slider5 .sliderRight');
    const slider5Left = document.querySelector('#slider5 .sliderLeft');
    const card5 = document.querySelectorAll('#slider5 .card600x800');
    const sliderDiv5 = document.querySelector('#slider5 .sider_container');
    const innerSlider5 = document.querySelector('#slider5 .innerSlider');
    const marginLeft5 = document.querySelector('#slider5 .firstChildMargin');
    const marginLeftCalNumber5 = functionGetLeftMargin(marginLeft5);
    const totalDivWidth5 = innerSlider5.offsetWidth;
    const cardWidth5 = card5[0].offsetWidth;
    const gapWidthNumber5 = functionGetGapWidth(innerSlider5);

    const slider5obj = {
        leftOverRightEnd: 0,
        disableSnapOnScroll: false,
    }
    slider5Right.addEventListener('click', () => {
        functionForRightClick(slider5obj, slider5Left, slider5Right, sliderDiv5, marginLeftCalNumber5, totalDivWidth5, cardWidth5, gapWidthNumber5);
    })
    slider5Left.addEventListener('click', () => {
        functionForLeftClick(slider5obj, slider5Left, slider5Right, sliderDiv5, marginLeftCalNumber5, slider5, gapWidthNumber5, cardWidth5);
    })
    sliderDiv5.addEventListener('scroll', () => {
        functionForScroll(slider5obj, slider5Left, slider5Right, sliderDiv5, slider5, totalDivWidth5, marginLeftCalNumber5);
    })

   /* For all Sliders */

    window.addEventListener('resize', () => {
        functionForResize(slider1Left, slider1Right, sliderDiv1, totalDivWidth1);
        totalDivWidth1 = innerSlider1.offsetWidth;
        cardWidth1 = card1[0].offsetWidth;
        gapWidthNumber1 = functionGetGapWidth(innerSlider1);

        functionForResize(slider2Left, slider2Right, sliderDiv2, totalDivWidth2);
        totalDivWidth2 = innerSlider2.offsetWidth;
        cardWidth2 = card2[0].offsetWidth;
        gapWidthNumber2 = functionGetGapWidth(innerSlider2);

        functionForResize(slider3Left, slider3Right, sliderDiv3, totalDivWidth3);
        totalDivWidth3 = innerSlider3.offsetWidth;
        cardWidth3 = card3[0].offsetWidth;
        gapWidthNumber3 = functionGetGapWidth(innerSlider3);

        functionForResize(slider4Left, slider4Right, sliderDiv4, totalDivWidth4);
        totalDivWidth4 = innerSlider4.offsetWidth;
        cardWidth4 = card4[0].offsetWidth;
        gapWidthNumber4 = functionGetGapWidth(innerSlider4);

        functionForResize(slider5Left, slider5Right, sliderDiv5, totalDivWidth5);
        totalDivWidth5 = innerSlider5.offsetWidth;
        cardWidth5 = card5[0].offsetWidth;
        gapWidthNumber5 = functionGetGapWidth(innerSlider5);
    });

}

export default scriptsJs;